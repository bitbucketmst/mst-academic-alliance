<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Workshop Registration | Thank You</title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="/css/style-register.css">
  <?=$pageData['header']?>
</head>

<body>
  <?=$pageData['body-first']?>
  <div class="form">
    <div>
    <a href="/"><img src="images/mst-mastek-logo.png" class="logo" height="81" width="137" /></a>
      <h1>Workshop on Salesforce</h1>
    </div>


    <div class="button text-center">
      Thank you for registering with us
    </div>

    <br/>
    <br/>
    <br/>

    <h2>Please check your inbox for a confirmation email</h2>

  </div>
  <?=$pageData['body-last']?>
  <script src="/script/script-register.js"></script>
</body>

</html>