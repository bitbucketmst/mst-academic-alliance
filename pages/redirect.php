<!DOCTYPE>
<html>
<head>
    <title><?=$pageData['title']?> | Redirection</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css?family=Tillana|Amatic+SC:700|Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <?=$pageData['header']?>

</head>
<body>
    <div id="content">
        <h2>
            <a class="title" href="/">Workshop on Salesforce</a>
        </h2>
        <?=$pageData['body-first']?>

    <?php

    $redirect_url = $redirectPages[$currentPageName];

    echo "<meta http-equiv='Refresh' content='2; url=$redirect_url'>";
    echo "You will be redirected to the requested site in <span id='redirect_seconds'>4</span> seconds<br/><br/>";
    echo "You can also click <a href='$redirect_url'>here</a> to redirect";

    echo "<br/><br/><br/><br/>";
    echo "Check out our <a href='https://www.mstsolutions.com' target='_blank'>company website</a>.";
    ?>


    </div>

    <?=$pageData['body-last']?>
    <script>
        $(document).ready(function() {
            countredirect($("#redirect_seconds").text());
        });
    </script>
</body>
</html>