<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Registration Form</title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="/css/style-register.css">
  <?=$pageData['header']?>
</head>
<body>
  <?=$pageData['body-first']?>
  <div class="form">
    <div>
    <a href="/"><img src="images/mst-mastek-logo.png" class="logo" height="81" width="137" /></a>
      <h1>Workshop on Salesforce</h1>
    </div>
    <ul class="tab-group">
      <li class="tab active">
        <a href="#student">Student</a>
      </li>
      <li class="tab">
        <a href="#staff">Staff</a>
      </li>
    </ul>
    <div class="tab-content">
      <div id="student">
        <form action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
          <input type=hidden name="oid" value="00D1U000000Gc8e">
          <input type=hidden name="retURL" value="http://workshop.mstsolutions.com/thank-you/">
          <input type=hidden name="lead_source" value="Web">
          <input type=hidden name="recordType" value="0121U000000QB40">
          <!--Students RT-->

          <div class="top-row">
            <div class="field-wrap">
              <label for="first_name">
                First Name
                <span class="req">*</span>
              </label>
              <input type="text" id="first_name" maxlength="40" name="first_name" size="20" required autocomplete="off" />
            </div>

            <div class="field-wrap">
              <label for="last_name">
                Last Name
                <span class="req">*</span>
              </label>
              <input type="text" id="last_name" maxlength="80" name="last_name" size="20" required autocomplete="off" />
            </div>
          </div>

          <div class="field-wrap">
            <label for="00N1U000008pwhY" class="active">
              DOB
            </label>
            <input type="date" autocomplete="off" />
            <input type="hidden" id="00N1U000008pwhY" name="00N1U000008pwhY" autocomplete="off" />
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label for="email">
                Email Address
                <span class="req">*</span>
              </label>
              <input type="email" id="email" maxlength="80" name="email" size="20" required autocomplete="off" />
            </div>

            <div class="field-wrap">
              <label for="mobile">
                Mobile
                <span class="req">*</span>
              </label>
              <input id="mobile" maxlength="10" name="mobile" size="10" type="text" pattern="\d*" title="Only numbers allowed" required autocomplete="off" />
            </div>
          </div>

          <div class="field-wrap">
            <label for="street">
              Address
            </label>
            <textarea rows="3" id="street" name="street"></textarea>
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label for="city">
                City
                <span class="req">*</span>
              </label>
              <input id="city" name="city" type="text" required autocomplete="off" />
            </div>

            <div class="field-wrap">
              <label for="state">
                State
                <span class="req">*</span>
              </label>
              <input id="state" name="state" type="text" required autocomplete="off" />
            </div>
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label for="country">
                Country
                <span class="req">*</span>
              </label>
              <input id="country" name="country" maxlength="40" type="text" required autocomplete="off" />
            </div>

            <div class="field-wrap">
              <label for="zip">
                Pincode
              </label>
              <input id="zip" name="zip" type="text" maxlength="6" pattern="\d*" title="Only numbers allowed" autocomplete="off" />
            </div>
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label for="00N1U000008pujW">
                Degree
                <span class="req">*</span>
              </label>
              <input id="00N1U000008pujW" name="00N1U000008pujW" required type="text" autocomplete="off" />
            </div>
            <div class="field-wrap">
              <label for="00N1U000008pumu">
                Department
                <span class="req">*</span>
              </label>
              <input id="00N1U000008pumu" name="00N1U000008pumu" required type="text" autocomplete="off" />
            </div>
          </div>

          <div class="field-wrap">
            <label for="00N1U000008ptqM">
              Interested Domain
              <span class="req">*</span>
            </label>
            <input id="00N1U000008ptqM" name="00N1U000008ptqM" required type="text" autocomplete="off" />
          </div>

          <div class="field-wrap">
            <label for="00N1U000008pun9">
              Trailhead Profile URL
              <span class="req">*</span>
            </label>
            <input id="00N1U000008pun9" name="00N1U000008pun9" required type="url" autocomplete="off" />
          </div>

          <div class="field-wrap">
            <label for="00N1U000008pswn" class="m-l-20">
              Willing to work in Software Industry
            </label>
            <input id="00N1U000008pswn" name="00N1U000008pswn" type="checkbox" value="1" autocomplete="off" />
          </div>

          <div class="field-wrap">
            <label for="00N1U000008psws" class="m-l-20">
              Willing to work with us
            </label>
            <input id="00N1U000008psws" name="00N1U000008psws" type="checkbox" value="1" autocomplete="off" />
          </div>

          <div class="field-wrap">
            <label for="00N1U000008pumz" class="m-l-20">
              Have Passport
            </label>
            <input id="00N1U000008pumz" name="00N1U000008pumz" type="checkbox" value="1" autocomplete="off" />
          </div>

          <div class="field-wrap">
            <label for="00N1U000008ptVd" class="active">
              Preferred Work Location
              <span class="req">*</span>
            </label>
            <select id="00N1U000008ptVd" name="00N1U000008ptVd">
              <option value="">--None--</option>
              <option value="Tiruchirappalli">Tiruchirappalli</option>
              <option value="Chennai">Chennai</option>
              <option value="Others">Others</option>
            </select>
          </div>

          <button type="submit" class="button button-block" />Submit</button>
        </form>
      </div>

      <div id="staff">
        <form action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
          <input type=hidden name="oid" value="00D1U000000Gc8e">
          <input type=hidden name="retURL" value="http://workshop.mstsolutions.com/thank-you/">
          <input type=hidden name="lead_source" value="Web">
          <input type=hidden name="recordType" value="0121U000000QB45">
          <!--Staff RT-->

          <div class="top-row">
            <div class="field-wrap">
              <label for="first_name">
                First Name
                <span class="req">*</span>
              </label>
              <input type="text" id="first_name" maxlength="40" name="first_name" size="20" required autocomplete="off" />
            </div>

            <div class="field-wrap">
              <label for="last_name">
                Last Name
                <span class="req">*</span>
              </label>
              <input type="text" id="last_name" maxlength="80" name="last_name" size="20" required autocomplete="off" />
            </div>
          </div>

          <div class="field-wrap">
            <label for="00N1U000008pwhY" class="active">
              DOB
            </label>
            <input type="date" autocomplete="off" />
            <input type="hidden" id="00N1U000008pwhY" name="00N1U000008pwhY"  autocomplete="off" />
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label for="email">
                Email Address
                <span class="req">*</span>
              </label>
              <input type="email" id="email" maxlength="80" name="email" size="20" required autocomplete="off" />
            </div>

            <div class="field-wrap">
              <label for="mobile">
                Mobile
                <span class="req">*</span>
              </label>
              <input id="mobile" maxlength="10" name="mobile" size="10" pattern="\d*" title="Only numbers allowed" type="text" required autocomplete="off" />
            </div>
          </div>

          <div class="field-wrap">
            <label for="street">
              Address
            </label>
            <textarea rows="3" id="street" name="street"></textarea>
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label for="city">
                City
                <span class="req">*</span>
              </label>
              <input id="city" name="city" type="text" required autocomplete="off" />
            </div>

            <div class="field-wrap">
              <label for="state">
                State
                <span class="req">*</span>
              </label>
              <input id="state" name="state" type="text" required autocomplete="off" />
            </div>
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label for="country">
                Country
                <span class="req">*</span>
              </label>
              <input id="country" name="country" maxlength="40"  type="text" required autocomplete="off" />
            </div>

            <div class="field-wrap">
              <label for="zip">
                Pincode
              </label>
              <input id="zip" name="zip" type="text" maxlength="6" pattern="\d*" title="Only numbers allowed" autocomplete="off" />
            </div>
          </div>

          <div class="field-wrap">
            <label for="00N1U000008punJ" class="active">
              Staff type
              <span class="req">*</span>
            </label>
            <select id="00N1U000008punJ" name="00N1U000008punJ" required>
              <option value="">--None--</option>
              <option value="Teaching">Teaching</option>
              <option value="Non-Teaching">Non-Teaching</option>
              <option value="Others">Others</option>
            </select>
          </div>

          <div class="field-wrap">
            <label for="00N1U000008punO" class="active">
              In profession from
            </label>
            <input type="date" autocomplete="off" />
            <input id="00N1U000008punO" name="00N1U000008punO" type="hidden" autocomplete="off" />
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label for="00N1U000008punE">
                Designation
                <span class="req">*</span>
              </label>
              <input id="00N1U000008punE" name="00N1U000008punE" type="text" required autocomplete="off" />
            </div>

            <div class="field-wrap">
              <label for="00N1U000008pumu">
                Department
                <span class="req">*</span>
              </label>
              <input id="00N1U000008pumu" name="00N1U000008pumu" type="text" required autocomplete="off" />
            </div>
          </div>

          <button type="submit" class="button button-block">Submit</button>
        </form>
      </div>

    </div>
    <!-- tab-content -->

  </div>
  <!-- /form -->
  <?=$pageData['body-last']?>
  <script src="/script/script-register.js"></script>
</body>

</html>