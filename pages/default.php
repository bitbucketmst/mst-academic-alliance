<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title><?=$pageData['title']?></title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="css/style-register.css">
  <?=$pageData['header']?>
</head>

<body>
  <?=$pageData['body-first']?>
  <div class="form">
    <div>
      <a href="/"><img src="/images/mst-mastek-logo.png" class="logo" height="81" width="137" /></a>
      <h1>Workshop on Salesforce</h1>
    </div>


    <div class="button text-center">
      Welcome to Our <br/> Workshop Website
    </div>

    <br/>
    <br/>
    <br/>
    <h2>Are you trying to register with us? <a href="register">Click Here</a></h2>
    <h3>Or, enter a complete URL to redirect to the right page</h3>

    <br/>

    <h2>You can also check <a href="https://www.mstsolutions.com" target="_blank">our website</a></h2>

  </div>
  <?=$pageData['body-last']?>
  <script src="script/script-register.js"></script>
</body>

</html>