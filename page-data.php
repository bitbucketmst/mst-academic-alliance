<?php

$pageData['title'] = 'Workshop on Salesforce';

$pageData['body-last'] .= <<<'GA'
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78112196-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78112196-4');
</script>
GA;

$pageData['body-last'] .= <<<'JQuery'
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
JQuery;

$pageData['body-last'] .= <<<'Script'
<script src="/script/script.js"></script>
Script;

