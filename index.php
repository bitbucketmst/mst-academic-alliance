<?php
error_reporting(0);

$Page_Name = [
    'trailhead'       => 'redirect',
    'start-trailhead' => 'redirect',
    'hackathon'       => 'redirect',

    'register'        => 'register',
    'default'         => 'default',
    'thank-you'       => 'thankyou',
];

$otherPages = [

];

$redirectPages = [
    'trailhead'       => 'https://trailhead.salesforce.com/',
    'start-trailhead' => 'https://trailhead.salesforce.com/content/learn/modules/trailhead_playground_management/create-a-trailhead-playground',
    'hackathon'       => 'https://trailhead.salesforce.com/users/mst-academic-alliance/trailmixes/trailhead-hackathon
',
];

$domainName = $_SERVER['HTTP_HOST'];

$pageData = [
    'title'      => "",
    'header'     => "",
    'body-first' => "",
    'body-last'  => "",
];

// Analytics tracking
require_once "page-data.php";

echo '<base href="/" />';

$error = true; //If routing hasn't any problems, change this to FALSE

if (isset($_GET['Page_Name'])) {
    if (isset($Page_Name[strtolower($_GET['Page_Name'])])) {
        //Main Website
        $currentPageName = strtolower($_GET['Page_Name']);
        include_once "pages/" . $Page_Name[$currentPageName] . ".php";
        $error = false;
    } else if (isset($otherPages[strtolower($_GET['Page_Name'])])) {
        //Other Pages
        $otherPage     = $otherPages[strtolower($_GET['Page_Name'])];
        $innerPagesRaw = $_GET['otherQueryString'] ?? '';
        $innerPages    = str_replace("-", " ", strtolower($innerPagesRaw));
        $directory     = null;

        if (file_exists("$otherPage/$innerPages")) {
            $directory = "$otherPage/$innerPages";
        } else {
            if (file_exists("$otherPage")) {
                $directory = "$otherPage/";
            }
        }

        if ($directory) {
            $directoryContents = scandir($directory);
            $files             = preg_grep("/^index/", $directoryContents);
            if (count($files)) {
                $file = array_shift($files);
                include_once $directory . $file;
                $error = false;
            }
        }
    }
}

if ($error) {
    include_once "pages/" . $Page_Name['default'] . ".php";
}
